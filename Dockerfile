# syntax=docker/dockerfile:1
ARG BUILDX_TAG=latest
FROM docker/buildx-bin:$BUILDX_TAG as buildx

FROM docker:latest

COPY --from=buildx /buildx /usr/libexec/docker/cli-plugins/docker-buildx

RUN apk update && apk add git

RUN docker buildx version